'use strict';
~ function() {
	var 
		fruitHolder = document.getElementById('fruitHolder'),
		totalfruit = 24,
		fruitIncrement;


window.init = function (){	
	createDiv();
	playAnimation(); 
	
}

function playAnimation(){
	var tl = new TimelineMax();

	for(var i=0;i<=5;i++){
		var animtime = 5 + Math.random() * 3;
		var delayTime = -1 + Math.random() *2
		var rotate = 60 + Math.random()* 10;
		var elem= '.fruit';
		tl.staggerTo(elem,animtime,{y:'+=400', rotationZ:rotate, ease:Power2.easeInOut},delayTime,'-=0.5');
	}

}

function createDiv() {
	var tl = new TimelineMax();
	fruitIncrement = 1
    for (var i = 0; i < totalfruit; i++) {
		for(var j=0;j<=5;j++){
      	var rightPosition = (270 - Math.random() * 270) + 'px',
    		topPosition = (-87 + Math.random() * 20) + 'px',
			fruit = document.createElement('div');
			fruit.className= "fruit";
			fruit.id = "img"+(j+1);

			fruit.style.right = rightPosition;
			fruit.style.top = topPosition;		

			fruitHolder.appendChild(fruit);
			fruitIncrement;
			
				}
    }
	
  }


}();

