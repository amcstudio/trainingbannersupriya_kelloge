(() => {
/*!
GULP DEPENDENCIES
npm install -g gulp gulp-concat  gulp-uglify gulp-autoprefixer gulp-sass gulp-useref gulp-notify gulp-plumber gulp-zip gulp-bump gulp-confirm gulp-dom del fs path gulp4-run-sequence browser-sync

sudo npm link gulp gulp-concat  gulp-uglify gulp-autoprefixer gulp-sass gulp-useref gulp-notify gulp-plumber gulp-zip gulp-bump gulp-confirm gulp-dom del fs path gulp4-run-sequence browser-sync
*/

  const gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    useref = require('gulp-useref'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    zip = require('gulp-zip'),
    bump = require('gulp-bump'),
    confirm = require('gulp-confirm'),
    dom = require('gulp-dom'),
    


    del = require('del'),
    fs = require('fs'),
    path = require('path'),
    runSequence = require('gulp4-run-sequence'),
    browserSync = require('browser-sync');

  var json,
    datajson,
    filePath = path.basename(__dirname),
    developer,
    adServerSrc,
    adServerFTSrc,
    thisVersion,
    adServer,
    bannerWidth = 300,
    bannerHeight = 250,
    footTag,
    manifestFile,
    manifestTag,
    politeIndexTag,
    politeDir = './ft_polite',
    animationSrc;


  /*--------------------------------------------------
   CONCATINATE THE JAVASCRIPT INTO ONE MINIFIED FILE
  --------------------------------------------------*/
  function concatScripts(){
       var alljsFiles = ['src/js/vendor/*',
      'src/js/olsTween*.js', 'src/js/adservers.js',
      '!src/js/politeLoadAnimation.js'
    ],
      animationjsFile = 'src/js/animation.js';

    if (fs.existsSync(animationjsFile)) {
      alljsFiles.push(animationjsFile);
    }
    return gulp.src(alljsFiles)
      .pipe(plumber())
      .pipe(concat('main.js'))
      .pipe(uglify())
      .pipe(gulp.dest('deploy/js'))

      .pipe(notify({
        message: 'Scripts task complete'
      }));
  }
  
  function movePoliteScripts(){
      return gulp.src(
        './src/js/politeLoadAnimation.js', { allowEmpty:true}
    )
      .pipe(plumber())
      .pipe(uglify())
      .pipe(gulp.dest('deploy/js'))

      .pipe(notify({
        message: 'Scripts task complete'
      }));
  }

  function compileSass(){
       return gulp.src(['./src/scss/main.scss'])
      .pipe(plumber())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 3 versions'],
        cascade: false

      }))
      .pipe(gulp.dest('src/css'))
      .pipe(notify({
        message: 'Sass task complete'
      }))
  }

  /*--------------------------------------------------
CLEAN OUT THE DEPLOY FOLDER 
--------------------------------------------------*/

function clean(){
  del(['_publishZip/', './deploy/**/*', '.DS_Store', '.DS_Store?', '._*', '.Spotlight-V100', '.Trashes', 'ehthumbs.db', 'Thumbs.db']);
}

 function cleanSys() {
    return del(['./_publishZip/', './deploy/**/.*', './deploy/**/_*', './deploy/**/*.db'])
  }
  /*--------------------------------------------------
  WATCH FUNCTION
  --------------------------------------------------*/
  //gulp.series(inlinesource, bsReload)
function watchFiles(done){
    gulp.watch('src/scss/**/*.scss', compileSass);
  gulp.watch('src/js/**/*.js', gulp.series(concatScripts, movePoliteScripts));
    gulp.watch(['src/**/*'], moveFiles);

    gulp.watch('src/index.html',moveHTML);
  gulp.watch('src/index.html', bsReload);
    // gulp.watch('deploy/**/**/*', ['inlinesource', 'bs-reload']);

    done();
}
 

  /*--------------------------------------------------
  MOVE ALL ASSETS INTO THE DEPLOY FOLDER
  --------------------------------------------------*/
   function moveFiles(){
     return gulp.src(["src/*", 'src/img/*', 'src/fonts/*', 'src/css/fonts/*', 'src/css/*', '!src/scss/', '!src/index.html'], {
       base: './src',
       nodir: true
     })
       .pipe(plumber())
       .pipe(gulp.dest('deploy'));
   }
 


  /*--------------------------------------------------
  MOVE HTML IN TO THE THE DEPLOY FOLDER & UPDATE LINKS
  --------------------------------------------------*/
  
  function moveHTML(){
    return gulp.src('src/index.html')
      .pipe(useref())
      .pipe(gulp.dest('deploy'));
  }
 


/*--------------------------------------------------
Adserver code 
--------------------------------------------------*/
  function doHeadTags(answer) {
    datajson = JSON.parse(fs.readFileSync('./data.json'))

    switch (answer) {
      case "1":
        // Double Click Studio
        adServerSrc = datajson.doubleClick.cdn;
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.doubleClick.standardDC;
        break;
      case "2":

        // Double Click Studio - Polite Load
        adServerSrc = datajson.doubleClick.cdn;
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.doubleClick.polite;
        break;
      case "3":
        // DCM 
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.standard.adserverJs;
        break;
      case "4":
        adServerSrc = datajson.flashTalking.cdn;
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.standard.adserverJs;
        ftStandard();
        break;
      case "5":
        // FLASH TALKING RICH
        adServerFTSrc = datajson.flashTalking.cdn;
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.standard.adserverJs;
        ftPolite();

        break;
      case "6":
        // polite Load js DoubleClick
        adServerSrc = datajson.doubleClick.cdn;
        footTag = datajson.doubleClick.jsPolite;
        politeJs();
        // gulp.series('mk-dc-politeLoad');
        break;
      case "7":
        // polite Load js DoubleClick
        adServerSrc = datajson.sizmek.cdn;
        footTag = datajson.sizmek.jsPolite;
        sizmekPoliteLoad()
        
        break;
      default:
        footTag = "window.onload = init();";
        break;

    }
    writeAdserver(footTag, answer);
    insertDomBundle();


  }
// /*--------------------------------------------------
// Insert dom
// --------------------------------------------------*/


 function insertDomBundle(){
    return gulp.src('./src/index.html')
      .pipe(dom(function () {

        if (adServerSrc != undefined) {
          this.getElementById('adServerTag').setAttribute('src', adServerSrc);
        }

        if (animationSrc != undefined) {

          this.getElementById('animationTag').setAttribute('src', animationSrc);
        }

        if (filePath) {
          this.getElementsByTagName('title')[0].innerHTML = filePath;
        }
        return this;
      }))
      //

      .pipe(gulp.dest('./src/'));
      
  }
function writeAdserver(serverTag, ans) {

    fs.writeFile('./src/js/adservers.js', serverTag, function (err) { });
    fs.writeFile('./adservers/.server.txt', ans, function (err) { });
}

function server(done){
  json = JSON.parse(fs.readFileSync('./package.json'));
  var version = json.version.toString;

  if (json.version === '0.0.0') {
    gulp.src('src/**/*')
      .pipe(confirm({
        question: 'Ad Server? Press | Doubleclick: 1 | Doubleclick polite: 2 |  standard: 3 | FT standard: 4 | FT rich: 5 | DCS polite: 6 | Sizmek: 7',
        proceed: function (answer) {
          doHeadTags(answer);
          return true;
        }
      }))

  }
  done();


}




/*--------------------------------------------------
Developer Name
--------------------------------------------------*/

  var addDeveloper =  () => {
    fs.appendFile('.contributors', '\n' + developer + ' | v-' + thisVersion + ' | ' + datetime, function (err) { });
  }


  function assignDeveloper(){
    return gulp.src('src/**/*')
      .pipe(confirm({
        question: 'Developer name? (press enter if you are re-opening the same file)',
        proceed: function (answer) {
          developer = answer;
          return true;
        }
      }))

  }
  

  function createSourceArchive() {
    json = JSON.parse(fs.readFileSync('./package.json'))
    return gulp.src('src/**/*')
      .pipe(zip(filePath + "-versionArchive-v" + json.version + '.zip'))
      .pipe(gulp.dest('./_versionArchive/'));
  }

  /*--------------------------------------------------
 version increment
--------------------------------------------------*/
  function bumpVersion(done){
    gulp.src('./package.json')
      .pipe(bump({
        version: thisVersion
      }))
      .pipe(gulp.dest('./'));
      done();
  }

  function addVersion(){
    json = JSON.parse(fs.readFileSync('./package.json'))

    return gulp.src('src/**/*')
      .pipe(confirm({
        question: 'Version Number?, current version is ' + json.version + ' (press enter if you are re-opening the same file)',
        proceed: function (answer) {
          if (answer !== "" && answer !== json.version && json.version !== "0.0.0") {
            createSourceArchive();
          }
          thisVersion = answer || json.version;
          return true;
        }
      }))

   
  }
 
  // /*--------------------------------------------------
  // Browser Sync
  // --------------------------------------------------*/
  function applyBrowserSync(){
    browserSync({
      server: {
        baseDir: "./src"
      }
    });
  }

  function bsReload() {
    browserSync.reload();
  }


function doAllTasks(){
  runSequence([bumpVersion], [moveFiles, moveHTML, watchFiles], [concatScripts, movePoliteScripts, compileSass], applyBrowserSync);
}

function skipQuestions(){
  runSequence([moveFiles, moveHTML, watchFiles], [concatScripts, movePoliteScripts, compileSass], applyBrowserSync);
}
 
  // /*--------------------------------------------------
  // create Ft manifest g+ulp
  // --------------------------------------------------*/

  function ftStandard(){
    manifestTag = datajson.flashTalking.manifestStandard;

    fs.writeFile('src/manifest.js', manifestTag, function (err) { });
    
  }
  
  // /*--------------------------------------------------
  // create ft polite files
  // --------------------------------------------------*/

  function ftPolite(){
    if (!fs.existsSync(politeDir)) {
      fs.mkdirSync(politeDir);
    }

    manifestTag = datajson.flashTalking.manifestRich;

    manifestTag = replaceString(manifestTag, 'filePath', filePath);

    fs.writeFile('./ft_polite/manifest.js', manifestTag, function (err) { });
  
    politeMarkup();
  }
  
  function politeMarkup(){
    if (!fs.existsSync(politeDir)) {
      fs.mkdirSync(politeDir);
    }

    politeIndexTag = datajson.flashTalking.richIndexStyle;
    return gulp.src('./adservers/ft_rich/index.html')
      .pipe(dom(function () {

        this.getElementsByTagName('style')[0].innerHTML = politeIndexTag;
        if (adServerFTSrc != undefined) {
          this.getElementById('adServerTag').setAttribute('src', adServerFTSrc);
        }
        return this;
      }))

      .pipe(gulp.dest('./ft_polite'));
  }

  

  // DoubleClick polite load load images through js

  function politeJs(){

    if (!fs.existsSync('./src/js/politeLoadAnimation.js')) {

      return gulp.src('./adservers/dc_polite/politeLoadAnimation.js')

        .pipe(gulp.dest('./src/js'));
    }
    dcPolite()

  }

  function dcPolite(){
    var fileDir = 'src/js/animation.js',
      fileExists = fs.existsSync(fileDir);
    if (animationSrc === undefined && json.version === '0.0.0') {
      if (fileExists) {
        return del(fileDir);
      }
    }
  }



  //sizmekPoliteLoad

  function sizmekPoliteLoad(){
    if (!fs.existsSync('./src/js/EBLoader.js')) {

      return gulp.src('./adservers/sizmek/EBLoader.js')

        .pipe(gulp.dest('./src/js'));
    }
  }


  // /*--------------------------------------------------
  // gulp Resize
  // --------------------------------------------------*/

  function resize(){
    thisVersion = '1.0.0';
    runSequence([bumpVersion], [rmVersionArchive, getBannerWidth, getBannerHeight, assignDeveloper], [addDeveloper, resizeInSass, insertDomResize, skipQuestions]);
  }
  
  function rmVersionArchive(){
    return del(['./_versionArchive'])
  }
  
 function getBannerWidth(){
   json = JSON.parse(fs.readFileSync('./package.json'))
   datajson = JSON.parse(fs.readFileSync('./data.json'))

   return gulp.src('./src/')
     .pipe(confirm({
       question: 'Banner width? ',
       proceed: function (answer) {
         if (answer) {
           bannerWidth = answer;
         } else {
           bannerWidth = 300;
         }
         return true;
       }
     }))
 }


  function getBannerHeight() {
    json = JSON.parse(fs.readFileSync('./package.json'))
    datajson = JSON.parse(fs.readFileSync('./data.json'))
    return gulp.src('./src/')
      .pipe(confirm({
        question: 'Banner height? ',
        proceed: function (answer) {
          if (answer) {
            bannerHeight = answer;
          } else {
            bannerHeight = 250;
          }

          return true;
        }
      }))
  }
 

  function resizeInSass(){

    fs.writeFile('./src/scss/_settings.scss', '', function (err) { })
    fs.appendFile('./src/scss/_settings.scss', '\n' + '$width:' + bannerWidth + 'px;' + '\n' + '$height:' + bannerHeight + 'px;', function (err) { });

  }
  
  function insertDomResize(){
    return gulp.src('./src/index.html')
      .pipe(dom(function () {


        if (filePath) {
          this.getElementsByTagName('title')[0].innerHTML = filePath;
        }
        if (bannerWidth != undefined && bannerHeight != undefined) {
          this.getElementsByName('ad.size')[0].setAttribute('content', "width=" + bannerWidth + "\,height=" + bannerHeight);
        }
        return this;
      }))
      .pipe(gulp.dest('./src/'));
  }


  exports.assignDeveloper = assignDeveloper;
  exports.server = server;
  exports.createSourceArchive = createSourceArchive;
  exports.bumpVersion = bumpVersion;
  exports.addVersion = addVersion;
  exports.applyBrowserSync = applyBrowserSync;
  exports.moveFiles = moveFiles;
  exports.moveHTML = moveHTML;
  exports.insertDomBundle = insertDomBundle;
  exports.clean = clean;
  exports.skipQuestions = skipQuestions;
  exports.cleanSys = cleanSys;
  exports.concatScripts = concatScripts;
  exports.movePoliteScripts = movePoliteScripts;
  exports.compileSass = compileSass;
  exports.politeMarkup = politeMarkup;
  exports.watchFiles = watchFiles;
  exports.rmVersionArchive = rmVersionArchive;
  exports.getBannerWidth = getBannerWidth;
  exports.getBannerHeight = getBannerHeight;
  exports.resizeInSass = resizeInSass;
  exports.insertDomResize = insertDomResize;
  exports.resize = resize;
  

/*--------------------------------------------------
DEFAULT
--------------------------------------------------*/

  gulp.task('default', gulp.series(server, addVersion, assignDeveloper, (done) => {
    
    if (developer !== undefined) {
      addDeveloper();
    }
    doAllTasks();
    done();
  }));


  /*--------------------------------------------------
  pubishZip
  --------------------------------------------------*/
  gulp.task('publish', gulp.series(cleanSys, () => {
    json = JSON.parse(fs.readFileSync('./package.json'))
    var currentVersion = json.version;
    currentVersion = currentVersion.substr(0, currentVersion.lastIndexOf('.'));
    return gulp.src('./deploy/**/*', {
      base: './deploy'
    })

      .pipe(zip(filePath + "_V" + currentVersion + "_publish.zip"))

      .pipe(gulp.dest('./_publishZip'));

  }));

 
  /*--------------------------------------------------
     pubish FT Zip
     --------------------------------------------------*/
  
 
  gulp.task('publishft', gulp.series(cleanSys, (done) => {
    json = JSON.parse(fs.readFileSync('./package.json'))

    publishDeploy();
    publishBase()
    done();
  }));
 

  function publishDeploy() {
    return gulp.src(['./deploy/**/*'], {
      base: './deploy'
    })

      .pipe(zip(filePath.replace('.', '_') + "_rich.zip"))
      .pipe(gulp.dest('./_publishZip'));
  }

  function publishBase() {
    return gulp.src(['./ft_polite/**/*'], {
      base: './ft_polite'
    })

      .pipe(zip(filePath.replace('.', '_') + "_base.zip"))
      .pipe(gulp.dest('./_publishZip'));
  }


  // /*--------------------------------------------------
  // DATE TIME FUNCTION FOR ARCHIVE NAMING
  // --------------------------------------------------*/

  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"]
  var hrs = ["12am", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm", "12pm"]
  var currentdate = new Date();
  var datetime = currentdate.getDate() + " " + months[currentdate.getMonth()] + " " + currentdate.getFullYear() + " at " + hrs[currentdate.getHours()];


  // /*--------------------------------------------------
  // replace string with another
  // --------------------------------------------------*/
  function replaceString(target, search, replacement) {
    var str = target;
    return str.replace(new RegExp(search, 'g'), replacement);
  }

})();